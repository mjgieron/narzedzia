$( window ).on( "load", function() {
    let button_tab_names = [];
    $('.tab_buttons').children().each(function(index) {
        button_tab_names.push($(this).get(0).id);
    });

    hideAll(button_tab_names);
    $('.' + button_tab_names[0]).show();
    $('#' + button_tab_names[0]).addClass('active');

    $(button_tab_names).each(function(index, value) {
        $('#' + value).hover(function() {
            hideAll(button_tab_names);
            $(this).addClass('active');
            $('.' + value).show();
        });
    });
});

var hideAll = function(button_tab_names){
    $(button_tab_names).each(function(index, value) {
        $('#' + value).removeClass('active');
        $('.' + value).hide();
    });
};